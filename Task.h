// Simple task scheduler for Arduino

#ifndef _TASK_h
#define _TASK_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif

class Task{
	typedef void (*functionPtr)();

public:
	Task(uint32_t execTime, functionPtr funcToExec);
	void start(void);
	void stop(void);
	void run(void);
	void set_exec_time(uint32_t ms);

private:
	uint32_t _execTime;
	uint32_t _lastExecTime;
	bool _isRuning = false;
	functionPtr _funcToExec;
};


#endif

