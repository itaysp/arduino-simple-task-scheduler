// Simple task scheduler for Arduino
// Author: Itay Sperling

#include "Task.h"


Task::Task(uint32_t execTime, functionPtr funcToExec)
{
	_execTime = execTime;
	_isRuning = false;
	_lastExecTime = millis();
	_funcToExec = funcToExec;
}

void Task::start()
{
	_isRuning = true;
	_lastExecTime = millis();
}

void Task::stop()
{
	_isRuning = false;
}

void Task::set_exec_time(uint32_t ms)
{
	_execTime = ms;
}


void Task::run()
{
	if (this->_isRuning == true) {
		if ((millis() - (this->_lastExecTime) > this->_execTime))
		{
			this->_funcToExec();
			this-> _lastExecTime = millis();
		}
	}
}