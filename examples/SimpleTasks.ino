//Simple Task Scheduler
//Author: Itay Sperling

#include "Task.h"

//This task will be executed every 1000ms
//Avoid blocking inside the function
void led_one(){
	Serial.print("Led One "); Serial.println(millis());
}

//This task will be executed every 2000ms
//Avoid blocking inside the function
void led_two() {
	Serial.print("Led Two "); Serial.println(millis());
}

Task taskLedOne(1000, &led_one); //Run this task every 1000ms
Task taskLedTwo(2000, &led_two); //Run this task every 2000ms

void setup(){
	//Init Serial
	Serial.begin(115200);
	
	//Start the tasks
	taskLedOne.start();
	taskLedTwo.start();
}

void loop(){
	//Run the tasks every loop
	taskLedOne.run();
	taskLedTwo.run();
}
